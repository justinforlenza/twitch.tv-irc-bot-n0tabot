<!DOCTYPE html>
<html>
	<head>
    	<link type="text/css" rel="stylesheet" href="css/stylesheet.css"/>
        <link href='css/OpenSans.css' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="scripts/script.js"></script>
		<title>N0tABot Control Panel</title>
	</head>
	<body>
        	<div id="header" class = "container">
            	<div id="social"><a class="twitch" rel="external" href="http://twitch.tv/jusjoe99/chat" target="_blank"></a></div>
            	<div id="logo">
                	<a href="index.php"></a>
                </div>
            </div>
            <?
				
			
			?>
            <div id="content" class = "container">
            	<ul>
                    <li id="command">Twitter Command<li>
                    	<ul><li>
                    	<form action="database/twitter_update.php" target="_self">
                        <table width="100%" border="0">
            				<tr>
                				<td width="8%">Enable/Disable:</td>
                    			<td width="45%">
                    				<select name="status">
                            			<option></option>
                           			 	<option value="0">Disabled</option>
                            			<option value="1">Enabled</option>
                        			</select></td>
                    			<td width="4%"><input type="submit" value="Update"></td>
                    			<td></td>
                			</tr>
            			</table>
                        </form>
                        </li></ul>	
                    </li>
                    <li id="command">Youtube Command<li>
                    	<ul><li>
                    	<form action="database/youtube_update.php" target="_self">
                        <table width="100%" border="0">
            				<tr>
                				<td width="8%">Enable/Disable:</td>
                    			<td width="45%">
                    				<select name="status">
                            			<option></option>
                           			 	<option value="0">Disabled</option>
                            			<option value="1">Enabled</option>
                        			</select></td>
                    			<td width="4%"><input type="submit" value="Update"></td>
                    			<td></td>
                			</tr>
            			</table>
                        </form>
                        </li></ul>	
                    </li>
                   	<li id="command">Texturepack Command
                    	<ul><li>
                    	<form action="database/resourcepack_update.php" target="_self">
                        <table width="100%" border="0">
            				<tr>
                				<td width="8%">Enable/Disable:</td>
                    			<td width="45%">
                    				<select name="status">
                            			<option></option>
                           			 	<option value="0">Disabled</option>
                            			<option value="1">Enabled</option>
                        			</select></td>
                    			<td width="4%"></td>
                    			<td></td>
                			</tr>
                            <tr>
                                <td width="8%">Command Message:</td>
                    			<td width="45%"><textarea maxlength ="140" name="msg" rows="5" cols="30"></textarea></td>
                    			<td width="4%"><input type="submit" value="Update"></td>
                    			<td></td>
                            </tr>
            			</table>
                        </form>
                        </li></ul>	         
                    </li>
					<li id="command">Game Command
                    	<ul><li>
                    	<form action="database/game_update.php" target="_self">
                        <table width="100%" border="0">
            				<tr>
                				<td width="8%">Enable/Disable:</td>
                    			<td width="45%">
                    				<select name="status">
                            			<option></option>
                           			 	<option value="0">Disabled</option>
                            			<option value="1">Enabled</option>
                        			</select></td>
                    			<td width="4%"></td>
                    			<td></td>
                			</tr>
                            <tr>
                                <td width="8%">Command Message:</td>
                    			<td width="45%"><textarea maxlength ="140" name="msg" rows="5" cols="30"></textarea></td>
                    			<td width="4%"><input type="submit" value="Update"></td>
                    			<td></td>
                            </tr>
            			</table>
                        </form>
                        </li></ul>	         
                    </li>
				</ul>
            </div>

</body>
</html>