import socket
import StringIO
import MySQLdb

ircsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server = "irc.twitch.tv"
channel = "#jusjoe99"
botName = "N0tABot"
oauth = ""
twitter = {"msg": "Check me out on me out on my Twitter to stay up to date and see when ill be live next! twitter.com/jusjoe99", "enabled": True}
youtube = {"msg": "Check me out on me out on the YouTubes, I dint post much but I plan to eventually doing some vids! youtube.com/jusjoe99", "enabled": True}
game = {"msg": "", "enabled":False}
resourcePack = {"msg":"", "enabled":False}
data = []
db = MySQLdb.connect(host = "localhost",user="myBot",passwd = "wHZ4VCWe2jaYxH9y",db = "mybot")
db.autocommit(True)
cursor = db.cursor()
cursor.execute("SELECT * FROM commands")
data = cursor.fetchall()
db.commit()
cursor.close()
db.close()

def MySQL():
	global data
	db = MySQLdb.connect(host = "localhost",user="myBot",passwd = "wHZ4VCWe2jaYxH9y",db = "mybot")
	cursor = db.cursor()
	cursor.execute("SELECT * FROM commands")
	data = cursor.fetchall()
	db.commit()
	cursor.close()
	db.close()

def ping():
	ircsock.send("PONG : Pong\n")

def joinChannel(chan):
	ircsock.send("JOIN " + chan +"\n")
	
def sendMessage(chan, msg):
	ircsock.send("PRIVMSG " + chan + " :" + msg + "\n")

def hello():
	ircsock.send("PRIVMSG " + channel + " :Hello\n")

def start():
	ircsock.connect((server, 6667))
	ircsock.send("PASS " + oauth + "\r\n")
	ircsock.send("USER "+ botName + botName + botName + "\r\n")
	ircsock.send("NICK " + botName + "\r\n")
	joinChannel(channel)
	MySQL()
	updateValues()
	
def getSong():
	song = open("cur_song.txt","r")
	cursong = song.read()
	ircsock.send("PRIVMSG " + channel + " :" + "The Current song is: " + cursong + "\n")
	song.close()

def updateValues():
	MySQL()
	checkTwitter()
	checkYouTube()
	checkGame()
	checkResourcePack()

def checkTwitter():
	twit = data[2]
	twitter["enabled"] = twit[1]

def checkGame():
	gme = data[0]
	game["msg"] = gme[2]
	game["enabled"] = gme[1]

def checkResourcePack():
	rp = data[1]
	resourcePack["msg"] = rp[2]
	resourcePack["enabled"] = rp[1]

def checkYouTube():
	ytube = data[3]
	youtube["enabled"] = ytube[1]

start()

while 1:
	ircmsg = ircsock.recv(2048)
	ircmsg = ircmsg.strip('\n\r')
	print ircmsg
	updateValues()
	if ircmsg.find(":!song") !=-1:
		getSong()
	if ircmsg.find(":!twitter") !=-1 and twitter["enabled"]:
		sendMessage(channel, twitter["msg"])
	if ircmsg.find("PING :") != -1:
		ping()
	if ircmsg.find(":!youtube") !=-1 and youtube["enabled"]:
		sendMessage(channel, youtube["msg"])
	if ircmsg.find("!tp") !=-1 and resourcePack["enabled"]:
		sendMessage(channel, resourcePack["msg"])
	if ircmsg.find("!playing") !=-1 and game["enabled"]:
		sendMessage(channel, game["msg"])
	
